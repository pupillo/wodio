Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2017-03-09T07:32:11+01:00

====== Argparse ======
Created Thursday 09 March 2017

===== Argparse =====
	* https://docs.python.org/3/library/argparse.html		# Parser for CLI options, args and commands
	* https://docs.python.org/3/howto/argparse.html#id1

===== Multiline help =====
	* http://stackoverflow.com/questions/3853722/python-argparse-how-to-insert-newline-in-the-help-text
		{{./pasted_image.png?height=100&href=.%2Fpasted_image.png}}
		# Note: help text is wrapped in paragraphs to fit the terminal width
	* https://bugs.python.org/issue13041		# argparse: terminal width is __not detected properly__
