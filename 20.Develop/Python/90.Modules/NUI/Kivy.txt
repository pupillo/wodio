Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2017-04-27T13:52:42+02:00

====== Kivy ======
Created Donnerstag 27 April 2017

===== About =====
	* https://kivy.org
		{{./pasted_image.png?height=80&href=.%2Fpasted_image.png}}
	* https://github.com/kivy/kivy-designer

===== Dependencies =====
	* Needs **pygame**  1.9.2x
	* typically instaled with **pip** 

===== Python issues =====
	* http://stackoverflow.com/questions/22929345/using-kivy-with-python-3-4
	* https://kivy.org/docs/installation/installation-linux.html
	* https://askubuntu.com/questions/457719/how-to-install-kivy-under-ubuntu
	* http://packages.ubuntu.com/trusty/python/python-kivy
	* http://packages.ubuntu.com/search?keywords=python3-kivy

	* http://packages.ubuntu.com/search?keywords=python3-kivy		# **Ubuntu 16**, Xenial

===== Tutorial =====
	* http://inclem.net/2014/01/09/kivy-crash-course/1_making-a-simple-app/
	* https://www.youtube.com/watch?v=F7UKmK9eQLY
	* https://pythonprogramming.net/kivy-drawing-application-tutorial/		# Wahnsinn

===== Kivi designer =====
	* http://kivy-designer.readthedocs.io/en/latest/quickstart.html
		# https://readthedocs.org/projects/kivy-designer/downloads/pdf/latest/	# pdf


===== Builder, .kv files =====
	* declarative building language, describing property bindings
	* comparable to: QML 
		# http://doc.qt.io/qt-5/qtquick-usecase-userinput.html





