Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2017-05-16T15:33:02+02:00

====== FullScreen ======
Created Dienstag 16 Mai 2017

===== About =====
	* http://stackoverflow.com/questions/5234434/simple-way-to-toggle-fullscreen-with-f11-in-pygtk
	* http://stackoverflow.com/questions/13658460/how-to-change-the-layout-of-a-gtk-application-on-fullscreen?rq=1


==== Gdk.WindowState ====
	* http://stackoverflow.com/questions/38113876/how-to-make-a-gtk3-window-full-screen
		{{./pasted_image.png?height=80&href=.%2Fpasted_image.png}}
	* https://lazka.github.io/pgi-docs/Gdk-3.0/flags.html#Gdk.WindowState
	* http://stackoverflow.com/questions/13586006/how-to-toggle-full-screen-mode-in-gtk-gtk

