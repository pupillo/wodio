#!/usr/bin/env python3
#-*- coding:utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from gi.repository import Gdk

class MyWindow(Gtk.Window):
    """Simple example for 3 buttons and toggle fullscreen mode.
    Button press events are mentioned on stdout.
    Launch the demo via command line.
    """
    
    def __init__(self):
        #~ Gtk.Window.__init__(self, title="Hello World", default_width=200)
        super().__init__(title="Hello World", default_width=200)

        self.__is_fullscreen = False

        self.box = Gtk.Box(spacing=6, orientation=Gtk.Orientation.VERTICAL)
        self.add(self.box)

        self.button1 = Gtk.Button(label="Hello")
        self.button1.connect("clicked", self.on_button1_clicked)
        self.box.pack_start(self.button1, expand=True, fill=True, padding=0)

        self.button3 = Gtk.Button(label="Toggle full screen")
        self.button3.connect("clicked", self.on_button3_clicked)
        self.box.pack_start(self.button3, expand=True, fill=True, padding=0)

        self.button2 = Gtk.Button(label="Goodbye")
        self.button2.connect("clicked", self.on_button2_clicked)
        self.box.pack_start(self.button2, True, True, 0)

        self.connect("window-state-event", self.__on_window_state_event)

    def on_button1_clicked(self, widget):
        print("Hello")

    def __on_window_state_event(self, widget, ev):
        self.__is_fullscreen = bool(ev.new_window_state & Gdk.WindowState.FULLSCREEN)

    def on_button3_clicked(self, widget):
        if self.__is_fullscreen:
            self.unfullscreen()
        else:
            self.fullscreen()
        print("Toggle Full Screen")

    def on_button2_clicked(self, widget):
        print("Goodbye")
        Gtk.main_quit()

def main():
    win = MyWindow()
    win.connect("delete-event", Gtk.main_quit)

    win.show_all()
    Gtk.main()

if __name__ == '__main__':
    main()










