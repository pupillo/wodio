Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2017-05-17T10:40:56+02:00

====== 7.1.EntryWindow ======
Created Mittwoch 17 Mai 2017

===== About =====
	* http://python-gtk-3-tutorial.readthedocs.io/en/latest/entry.html#example
	* [[./entry_window.py]]

===== GTK =====
	* https://developer.gnome.org/gtk3/stable/api-index-full.html
	* https://developer.gnome.org/gtk3/stable/GtkEntry.html#GtkEntry-struct
	* https://developer.gnome.org/gtk3/stable/GtkEntry.html#GtkEntry-activate	# ENTER
	* https://developer.gnome.org/gtk3/stable/GtkEntry.html#GtkEntry.object-hierarchy

===== PGI =====
	* https://lazka.github.io/pgi-docs/Gtk-3.0/classes/Entry.html
		{{./pasted_image.png?height=80&href=.%2Fpasted_image.png}}

