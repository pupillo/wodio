#!/usr/bin/env python3
#-*- coding:utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import GObject
import os

# mini display has LINES lines
LINES=10

class Gui():
    """Simple example for catching key press and other events.
    Note: If there is a window manager, expect some key press events or other
    events being catched before the app has any chance to see them.
    Those may be transformed into new events like "delete-event", which is
    generated via x-button on the window border or ALT-F4, again interpreted by
    the window manager. This is a WM configuration issue.

    Note also: Events may be delivered to multiple widgets if needed.
    And: the widget identifier used in glade may be looked up via the
    method Gtk.Buildable.get_name().
    """
    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file('key_catcher.xml')
        self.builder.connect_signals(self)
        
        self.window_main = self.builder.get_object('window_main')
        self.text_entry = self.builder.get_object('text_entry')
        self.label_echo = self.builder.get_object('label_echo')
        #~ self.window_main.connect("delete-event", self.on_delete)
        self.window_main.show()

    def say(self, msg):
        if not hasattr(self, "say_prev_line"):
            self.say_prev_line = ""
        print(msg)
        self.label_echo.set_text(self.say_prev_line + "\n" + msg)
        self.say_prev_line = msg

    def key_press_event(self, widget, event):
        keyval_name = Gdk.keyval_name(event.keyval)
        glade_oid = Gtk.Buildable.get_name(widget)
        self.say("Key pressed: {}, {}, {}".format(widget, keyval_name, glade_oid))
        return False

    def button_press_event(self, widget, event):
        keyval_name = Gdk.keyval_name(event.keyval)
        self.say("Key pressed: {}, {}".format(widget, keyval_name))
        return False

    def delete_event(self, widget, event):
        self.say("Delete event found, {}, {}".format(widget, event))
        return True
        #~ return False
    
    def on_clear(self, widget):
        print("on_clear found.")
        self.label_echo.set_text("Clean 1")
        self.text_entry.set_text("Clean 2")

    def on_quit(self, widget):
       Gtk.main_quit()

def main():
    win = Gui()
    Gtk.main()

if __name__ == '__main__':
    main()










