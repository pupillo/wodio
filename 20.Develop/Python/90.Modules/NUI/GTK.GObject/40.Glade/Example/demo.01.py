#!/usr/bin/env python3
#-*- coding:utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import GObject
import os

class Gui():
    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file('demo.01.glade')
        self.builder.connect_signals(self)

        self.win = self.builder.get_object('window1')
        self.label = self.builder.get_object('display_count')
        self.label2 = self.builder.get_object('label2')
        self.edit_field = self.builder.get_object('entry1')
        self.win.connect("delete-event", Gtk.main_quit)
        self.win.show()
        self.count = 0

    def on_count(self, widget):
        print("Added one to total")
        self.count += 1
        self.label.set_text(str(self.count))

    def on_ok(self, widget):
        contents = self.edit_field.get_text()
        self.label2.set_text(contents)
        print("Ok pressed; contents = ({})".format(contents))
    
    def on_clear(self, widget):
        print("Clear counter")
        self.count =0
        self.label.set_text(str(self.count))
        self.label2.set_text("")
        self.edit_field.set_text("clean.")
        self.on_ok(widget)

    def on_cancel(self, widget):
        Gtk.main_quit()


def main():
    win = Gui()
    #~ win.show_all()
    Gtk.main()

if __name__ == '__main__':
    main()










