from gi.repository import Gtk
import os, sys  

class GUI:

image = 0;

def __init__(self):

    self.window = Gtk.Window()
    self.window.set_title ("Imagine")
    self.window.connect_after('destroy', self.destroy)
    self.window.set_border_width(10)
    self.window.set_default_size(400, 200)

    self.window.vbox = Gtk.Box()
    self.window.vbox.set_spacing (5)
    self.window.vbox.set_orientation (Gtk.Orientation.VERTICAL)
    self.window.add (self.window.vbox)


    self.window.hbox = Gtk.ButtonBox()
    self.window.vbox.add(self.window.hbox)

    self.window.new_button = Gtk.Button(label="New")
    self.window.new_button.connect("clicked", self.on_new_clicked)
    self.window.hbox.pack_start(self.window.new_button, False, False, 0)

    self.window.save_button = Gtk.Button(label="Save")
    self.window.save_button.connect("clicked", self.on_save_clicked)
    self.window.hbox.pack_start(self.window.save_button, False, False, 0)

    self.window.email_button = Gtk.Button(label="eMail")
    self.window.email_button.connect("clicked", self.on_email_clicked)
    self.window.hbox.pack_start(self.window.email_button, False, False, 0)

    self.window.pen_button = Gtk.Button(label="Pen")
    self.window.pen_button.connect("clicked", self.on_pen_clicked)
    self.window.hbox.pack_start(self.window.pen_button, False, False, 0)

    self.image = Gtk.Image()
    self.window.vbox.pack_start (self.image, False, False, 0)

    self.window.show_all()

def on_save_clicked(self, widget):
    print("fixme: Save Clicked") 

def on_email_clicked(self, widget):
    print("fixme: eMail Clicked") 

def on_pen_clicked(self, widget):
    print("fixme: Pen Clicked") 

def on_new_clicked (self, button):
    print("fixme: New Clicked")#code below is just to test the image panel works
    dialog = Gtk.FileChooserDialog ("Open Image", button.get_toplevel(), Gtk.FileChooserAction.OPEN);
    dialog.add_button ("Cancel", 0)
    dialog.add_button ("Ok", 1)
    dialog.set_default_response(1)

    filefilter = Gtk.FileFilter ()
    filefilter.add_pixbuf_formats ()
    dialog.set_filter(filefilter)

    if dialog.run() == 1:
        self.image.set_from_file(dialog.get_filename())

    dialog.destroy()

def destroy(self, window):
    Gtk.main_quit()

class Controls:

    def send_email (self):
        print("fixme: Send Email")

    def pen_controls (self):
        print("fixme: Pen Controls") 


    def on_save_clicked(self, widget):
        print("fixme: Save Clicked") 


def main () :
    win = GUI()
    Gtk.main()

if __name__ == "__main__":
    sys.exit(main())
    
