Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2017-03-10T15:09:53+01:00

====== Yaml ======
Created Friday 10 March 2017

===== Pyyaml, python3-yaml =====
	* http://pyyaml.org/wiki/PyYAMLDocumentation
	* http://yaml.org/spec/1.1/#id857168		# YAML specs
		# http://pyyaml.org/wiki/PyYAMLDocumentation#DumpingYAML
		# ''yaml.dump(data, outfile, indent=4, default_flow_style=False)'' 
	* http://stackoverflow.com/questions/3790454/in-yaml-how-do-i-break-a-string-over-multiple-lines
		# continuation lines, long lines
	* https://learnxinyminutes.com/docs/yaml/		# __super short overview__ 

	**Criticism of YAML**
	* http://stackoverflow.com/questions/2451732/how-is-it-that-json-serialization-is-so-much-faster-than-yaml-serialization-in-p
