Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2017-06-21T07:14:05+02:00

====== Pyflakes ======
Created Wednesday 21 June 2017

===== Imported but unused =====
	* https://stackoverflow.com/questions/5033727/how-do-i-get-pyflakes-to-ignore-a-statement
	* https://stackoverflow.com/questions/5033727/how-do-i-get-pyflakes-to-ignore-a-statement/12121404#12121404
		{{./pasted_image002.png?height=80&href=.%2Fpasted_image002.png}}
	* https://www.laurivan.com/make-pyflakespylint-ignore-unused-imports/
		{{./pasted_image003.png?height=80&href=.%2Fpasted_image003.png}}	# **YES**! 


===== Flake8 =====
	* https://pypi.python.org/pypi/flake8
	* http://flake8.pycqa.org/en/latest/user/options.html
	* http://flake8.pycqa.org/en/latest/user/options.html#cmdoption-flake8-select
		{{./pasted_image001.png?height=80&href=.%2Fpasted_image001.png}}
	* https://stackoverflow.com/questions/23600614/how-to-use-flake8-for-python-3
		{{./pasted_image.png?height=80&href=.%2Fpasted_image.png}}
		''/usr/bin/python3 -m pyflakes foo.py''
	* http://flake8.pycqa.org/en/2.5.5/warnings.html
