Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2017-03-08T14:47:50+01:00

====== 20.Exceptions ======
Created Wednesday 08 March 2017

===== About =====
	* https://dzone.com/articles/everything-you-need-know-about
		{{./pasted_image.png?height=80&href=.%2Fpasted_image.png}}


===== Custom exceptions =====
	* https://stackoverflow.com/questions/1319615/proper-way-to-declare-custom-exceptions-in-modern-python
	* https://www.codementor.io/sheena/how-to-write-python-custom-exceptions-du107ufv9	# Blog, __Goood__ 
		{{./pasted_image001.png?height=80&href=.%2Fpasted_image001.png}}
	* http://www.codecalamity.com/exception-exception-read-all-about-it/#the-basic-try-except-block

