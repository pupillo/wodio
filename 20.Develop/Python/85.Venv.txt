Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2017-06-16T11:58:21+02:00

====== 85.Venv ======
Created Friday 16 June 2017

===== About virtual environment (venv) =====
	* https://pypi.python.org/pypi/virtualenv
	* http://python-guide-pt-br.readthedocs.io/en/latest/dev/virtualenvs/

===== Articles =====
	* https://stackoverflow.com/questions/23842713/using-python-3-in-virtualenv
	* https://www.sitepoint.com/virtual-environments-python-made-easy/

===== Python3 venv, builtin =====
	* https://docs.python.org/3/library/venv.html#module-venv
		# Ubuntu bug; ''python3 -m venv'' is available since **python3.4**


===== Virtualenv tool =====
	* https://virtualenv.pypa.io/en/latest/

