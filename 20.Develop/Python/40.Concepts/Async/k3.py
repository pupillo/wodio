#!/usr/bin/env python3
from gi.repository import Gtk,Gdk, GLib
import threading 
import time

class Gui(Gtk.Window):
  def __init__(self):
      self.Window = Gtk.Window()
      self.Window.set_border_width(8)
      self.Window.set_title("Некий GUI")
      self.Window.connect('destroy', lambda x: self.stop())

      self.outBut = Gtk.Button.new_from_stock(Gtk.STOCK_OK)
      self.outBut.set_size_request(150, 35)
      self.Window.connect('destroy', lambda x: self.stop())
      self.Window.add(self.outBut)

      self.Window.show_all()
      threading.Thread(target=loop_sleep).start()

  def stop(self):
      Gtk.main_quit()

  def passfun(self):
      pass

def loop_sleep():
      i = 1
      while True:
           print(i)
           i = i + 1
           time.sleep(.1)   # may be active or not.



app = Gui()
GLib.threads_init()
Gdk.threads_init()
Gdk.threads_enter()
Gtk.main()
Gdk.threads_leave()
