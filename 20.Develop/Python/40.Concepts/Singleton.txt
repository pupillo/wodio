Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2017-07-24T12:32:14+02:00

====== Singleton ======
Created Monday 24 July 2017

===== See also =====
	* [[Metaclass]]

===== About =====

'''
# =====
#!/usr/bin/env python3
#-*- coding:utf-8 -*-

class Singleton(object):
    _instance = None
    susi = 4711
    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = super().__new__(cls, *args, **kwargs)
        return cls._instance

if __name__ == '__main__':
    s1 = Singleton()
    s2 = Singleton()
    if (id(s1) == id(s2)):
        print("Same")
    else:
        print("Different")
    s1.susi = 4712
    print('s1.susi={}'.format(id(s1.susi)))
    print('Singleton.susi={}'.format(id(Singleton.susi)))
#..
'''

