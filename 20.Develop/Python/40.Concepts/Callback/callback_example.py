#!/usr/bin/env python3
#-*- coding:utf-8 -*-

import getpass

class MyClass():
    pwords = {}     # saved passwords
    def __init__(self):
        print('Hello world')

    @staticmethod
    def class_callback(user, ip):
        if ((user, ip) in MyClass.pwords):
            p = MyClass.pwords[(user, ip)]
            print('hit. ({}@{}): {}'.format(user, ip, p))
        else:
            p = getpass.getpass('{}@{}, password: '.format(user, ip))
            MyClass.pwords[(user, ip)] = p
            print('saved. ({}@{}): {}'.format(
                user, ip, MyClass.pwords[(user, ip)]))
        return p

def caller(func, user, ip):
    ret = func(user, ip)
    return ret

def main():
    (user, ip) = 'sesam', '1.2.3.4'
    print('call 1: {}'.format(caller(MyClass.class_callback, user, ip)))
    print('call 2: {}'.format(caller(MyClass.class_callback, user, ip)))
    mc = MyClass()
    print('call 3: {}'.format(caller(mc.class_callback, user, ip)))
    

if __name__ == '__main__':
    main()

