Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2015-10-12T15:36:29+02:00

====== RawCollection ======
Created Monday 12 October 2015

===== Wiki =====
	* https://en.wikibooks.org/wiki/Git
	* https://en.wikibooks.org/wiki/Git/Internal_structure
	* https://en.wikibooks.org/wiki/Git/Introduction
		# {{./pasted_image.png?height=80}}	# {{./pasted_image001.png?height=80}}	# 


===== Tutorials =====
	* http://www.ralfebert.de/tutorials/git/
	* http://marklodato.github.io/visual-git-guide/index-en.html

	# {{./pasted_image002.png?height=80}}		# {{./conventions.svg?height=80}}


===== GitGuys =====
	* http://www.gitguys.com/topics/the-git-object-model-starting-with-the-blob
	# {{./pasted_image003.png?height=80}}	# {{./pasted_image005.png?height=80}}	# {{./pasted_image004.png?height=80}}
	
	* http://www.gitguys.com/topics/what-is-the-format-of-a-git-blob/	# **gory details** 
	# {{./pasted_image006.png?height=80}}

	* **GitPro** 
	# {{./pasted_image007.png?height=80}}	# {{./pasted_image008.png?height=80}}	# {{./pasted_image009.png?height=80}}  # **colors only (!)** 

	#{{./pasted_image010.png?height=80}}	# {{./pasted_image011.png?height=80}}	# {{./pasted_image012.png?height=80}}
	
	# {{./pasted_image013.png?height=80}}	# {{./pasted_image014.png?height=80}}	# {{./pasted_image015.png?height=80}}	# {{./pasted_image016.png?height=80}}

	# {{./pasted_image017.png?height=80}}	# {{./pasted_image018.png?height=80}}	# {{./pasted_image019.png?height=80}}

	# {{./pasted_image020.png?height=80}}	# {{./pasted_image021.png?height=80}} 	# {{./pasted_image022.png?height=80}} 5-12 

	# {{./pasted_image023.png?height=80}}**overlaid symbols**		# {{./pasted_image024.png?height=80}} Fig 7-10	# {{./pasted_image025.png?height=80}} rebase 7-26

	# {{./pasted_image026.png?height=80}} e.g. terminal	# {{./pasted_image027.png?height=80}} __10-3 objects__ 	# {{./pasted_image028.png?height=80}} 10-4

	# {{./pasted_image029.png?height=80}} 5-26 large merges 	# {{./pasted_image033.png?height=80}}	__WORKFLOW__ 



===== File state =====
	# {{./pasted_image030.png?height=80}}	# {{./pasted_image031.png?height=80}} (**SUPER**) 	# {{./pasted_image032.png?height=80}}
	
	
	
	#











