Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2015-10-09T13:14:01+02:00

====== Background ======
Created Friday 09 October 2015

===== About =====
	* Document background is for interactive visibility only, it may not be printed.
	* Use a real rectangle for defining any background attributes (opacity, color, pattern, gradient etc).
	* If background rectangle exceeds page boundaries, it is cropped.
	* Place background onto a **separate layer** ("Background") and lock it.

