Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2015-11-14T12:10:44+01:00

====== 10.Bugs ======
Created Saturday 14 November 2015


===== Gtk, fonts =====
	* https://bugs.dogfood.paddev.net/inkscape/+bug/408653
		# Inkscape font: Courier -> Sans 
		# 0.46 included '/System/Library/Fonts' whereas recent prerelease builds do not: the pango library - the internal font rendering engine - doesn't handle some proprietary font formats very well (apples .dfont, .fon and others - many of the included system fonts on OS X), so the System Font Library got dropped ATM (SVN rev. 20240). This might well change again when newer versions of pango are available.
		# **Workaround**: see the FAQ on the Inkscape Wiki how you can convert *.dfont and other apple font formats to *.ttf files so that they are available in the Inkscape font list.
	* https://bugs.launchpad.net/inkscape/+bug/408653	# same bug


===== Ellipse even stroke width =====
	* http://www.inkscapeforum.com/viewtopic.php?t=11458
	* https://bugs.launchpad.net/inkscape/+bug/165715
		#  The result is that if you have a stroked shape that you have nonisotropically stretched, the path representation of the lines in Postscript format has different line thicknesses for the shape, depending
		on the orientation of the line relative to the direction of greatest stretch.
		# Fixed in inkscape > 0.49 
		# **WorkAround:** 
		# In Inkscape, convert the shape (ellipse) to a path (menu 'Path > Object to Path') and nudge it with the arrow keys (e.g. a step up and down again) to flatten the explicit transformation directly into the path data and node coordinates. After this, the stroke will be rendered uniformly in Inkscape as well as other viewers (and in cairo-based exports to EPS/PS/PDF).


===== Launchpad =====
	* https://bugs.launchpad.net/inkscape
	* https://inkscape.org/de/mitmachen/bugs-melden/

===== Bezier, arrow =====
	* http://www.inkscapeforum.com/viewtopic.php?t=8788

===== Nothing on the clipboard =====
	* http://www.inkscapeforum.com/viewtopic.php?t=7181
	* http://wiki.inkscape.org/wiki/index.php/Frequently_asked_questions#Copying_and_pasting_in_Inkscape_creates_pixellated_images_instead_of_copying_the_vector_objects
	* https://bbs.archlinux.org/viewtopic.php?id=113100	
		# I create a simple rect and press ctr+c (or use the context menu) and this error comes up:


===== Crash on Ubuntu 16.04 =====
'''
terminate called after throwing an instance of 'Glib::ConvertError'
#..
'''
	* https://bugs.launchpad.net/inkscape/+bug/1388100
		# Crash while opening any file
	* https://bugs.launchpad.net/inkscape/+bug/1404934
		# Crash when opening files (Glib::ConvertError exception)
		{{./pasted_image.png?height=100&href=.%2Fpasted_image.png}}


===== Clear inkscape recent files history =====
	* http://www.inkscapeforum.com/viewtopic.php?t=11462
	{{./pasted_image001.png?height=60&href=.%2Fpasted_image001.png}}
	''less ~/.local/share/recently-used.xbel'' 







