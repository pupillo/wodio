Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2015-06-25T14:07:52+02:00

====== 20.Preferences ======
Created Thursday 25 June 2015

===== About =====
	* **Preferred default**: plan for A4 size; Portrait, due to zim
		# WEB, zim books: keep in mind that material should fit
	* **Special**: A4 landscape
		# Use landscape for big diagrams.
		# think about scaling or previews in zim books and WEB.
		# landscape is to be viewed separately
		# **landscape** is the **exception**, portrait is the normal case
	* **Library, limits**: create objects or shapes to be used anywhere in any size
		# stay in a canvas of lower equal max: **180mm x 180mm**
		# they easily may be used in any A4 Portrait or Landscape drawings
	* **Snap**: Define a list of standard good snap spacing (.5, 1, 2mm)
	* **Grid**: Define good grid spacing (1, 2, 5, 10 mm)
CAST: 


===== Images =====
	* Use small preview images, A5 Landscape or A4 Portrait to keep pages manageable
	* Use image viewer (eog) to zoom and pan
	* Use Web browser for presentations
		# Zoom in to images separately where details of previews are of interest
	* Print docs from web browser, without the need of image rotation
		* Open question: What about image details within text? 
		* Open question: What about A4 landscape pictures?
		* Idea: separate **landscape** WEB page with collected contents to be printed?













