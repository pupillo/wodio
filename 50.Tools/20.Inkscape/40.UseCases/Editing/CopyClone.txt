Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2015-10-09T08:15:10+02:00

====== CopyClone ======
Created Friday 09 October 2015

===== About =====
	* http://booki.flossmanuals.net/inkscape/_draft/_v/1.0/copy-clone-duplicate/

===== Copy =====
	Ctrl + C


===== Paste =====
	Ctrl + V


===== Duplicate =====
	Ctrl + D		# copy selection onto itself, leave it selected


===== Clone =====
	Alt + D		# new object is linked to original (parent)
	.			# parent's attributes are maintained (color, fill, stroke, transparency etc)


===== Unlink clone =====
	Alt + Shift + D	# separate cloned objects from parent, make them stand alone


===== Select original =====
	Shift + D		# find and select the parent of a clone
