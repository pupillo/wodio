Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2016-05-10T08:51:33+02:00

====== Pdf ======
Created Tuesday 10 May 2016

===== SaveAS =====
	* File / Save As / pdf format;  	# that's all 2016-06-22

===== Print to file =====
	* Print Preview; Page-Setup	# {{./pasted_image.png?height=40}}
	* [[./output.pdf]] 				# Super quality

===== Inkscape, pdf files =====
	* Launch **Inkscape** from __page__ folder itself, not from __zim__
		# default: save **pdf** file in **current** directory
	* Print to file, pdf format
	* Save in folder: 
		a) Page folder is already selected
		b) give it the right name


===== Landscape =====
	* https://answers.launchpad.net/ubuntu/+source/inkscape/+question/53578
	* https://bugs.launchpad.net/ubuntu/+source/cups/+bug/47649
	* https://sourceforge.net/p/inkscape/mailman/message/20234207/
	* https://bugs.launchpad.net/inkscape/+bug/219640

===== Ubuntu, rotate pdf =====
	* http://makandracards.com/makandra/1487-rotate-a-pdf-under-ubuntu-linux
		# [[./RotatePdf.mht]] 
		# pdftk; pdfchain
		So, to rotate only page 7 of in.pdf 90 degrees clockwise:
		''pdftk in.pdf cat 1-6 7E 8-end output out.pdf''
		
		To rotate the entire document in.pdf by 180 degrees:
		''pdftk in.pdf cat 1-endS output out.pdf''

	* **Real example, ok**
		# ''pdftk ConProCus.pdf cat 1east output out.pdf'' 
		# ''pdftk GitLabWorkflow.pdf cat 1east output out.pdf'' 


===== Test result =====
	* ''evince'' 
	* ''xpdf'' 


