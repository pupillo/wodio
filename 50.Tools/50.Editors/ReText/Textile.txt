Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2017-05-12T07:38:49+02:00

====== Textile ======
Created Freitag 12 Mai 2017

===== Examples =====
	* https://txstyle.org/doc/2/paragraphs
	* https://github.com/retext-project/retext/wiki		# ReTex Wiki
		{{./pasted_image.png?height=80&href=.%2Fpasted_image.png}}, {{./pasted_image001.png?height=80&href=.%2Fpasted_image001.png}}


===== Bug reporting =====
	* https://github.com/retext-project/retext/wiki
	* https://github.com/retext-project/retext/issues

===== Solution =====
	* Menu / Edit / Use WebKit renderer
		{{./pasted_image002.png?height=100&href=.%2Fpasted_image002.png}}

