Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2015-03-13T07:33:28+01:00

====== 30.Icons ======
Created Friday 13 March 2015

===== About =====
	* Examples to use icons from a shared icons repository


===== Tar ball =====
{{/Icons/BeginInput.png}}
'''
cp -a $TAG_BASE/src/2x/analysis_tools/Loadmeter /tmp/Loadmeter
cd /tmp/Loadmeter
du -hs .
#..
'''
{{/Icons/BeginOutput.png}}
'''
76M	.
#..
'''
{{/Icons/BeginInput.png}}
'''
rm -fr Doc/Print*  Doc/Scratch*
#..
'''
{{/Icons/BeginInput.png}}
{{/Icons/BeginOutput.png}}
{{/Icons/BeginError.png}}

===== Idea.1 =====
	* document_root=~/info
	* Address icons this way:
		# ''/Icons/BeginError.png'' 


===== Idea.2 =====
	* document_root=/tmp/Zim.Document.root
	* add icons to somewhere else:
		# ''/home/awehler/ZimIcons''
	* Agree on shared symlink to given central icons repo
		# ''mkdir /tmp/Zim.Document.root''
		# ''ln -s /home/awehler/ZimIcons /tmp/Zim.Document.root''
	* Address icons this way:
	# ''/ZimIcons/BeginError.png'' 
	* We now need ''/tmp/Zim.Document.root''
		''/ZimIcons'' to exist,
		# test / create it on restart


===== Idea.3 (good) =====
'''
(2016-01-28 08:32:23) spies: DOCROOT should be: /tag/os/support/public/zim
(2016-01-28 08:32:43) spies: problem: /tag/os/support/public/.htaccess allows only access for us (up to now)
(2016-01-28 08:37:56) spies:  DOCROOT should be: ~/Noteboooks/docroot
(2016-01-28 08:38:10) spies: icons: ~/Noteboooks/docroot/icons
(2016-01-28 08:39:05) spies: ~/Noteboooks/docroot is a symlink to /tag/os/support/public/zim/docroot
'''


===== Agreed =====
'''
(2016-01-28 11:18:29) awehler: Agreed. This last suggestion is the best so far.
(2016-01-28 11:20:33) awehler: Anything else addressed relative within some notebook has massive drawbacks.
(2016-01-28 11:22:29) awehler: E.g. moving a page to some different hierarchy level breaks relative attachment links (say, links are not updated :-(   )
(2016-01-28 11:23:24) awehler: There is a bug report for this, open even for 0.65:  https://bugs.launchpad.net/zim/+bug/1498635
(2016-01-28 11:24:40) awehler: So icons must be realized with absolute paths, say, document_root must point outside of the notebook.
(2016-01-28 11:44:00) awehler: tree ~/Notebooks/
(2016-01-28 11:44:07) awehler: /homes/awehler/Notebooks/
(2016-01-28 11:44:07) awehler: └── docroot -> /tag/os/support/public/zim/docroot/
(2016-01-28 11:44:18) awehler: tree /tag/os/support/public/zim
(2016-01-28 11:44:28) awehler: /tag/os/support/public/zim
(2016-01-28 11:44:28) awehler: └── docroot
(2016-01-28 11:44:28) awehler:     └── icons
(2016-01-28 11:44:28) awehler:         ├── hdd.svg
(2016-01-28 11:44:28) awehler:         ├── led-aqua.gif
(2016-01-28 11:44:28) awehler:         ├── led-blue.gif
(2016-01-28 11:44:35) awehler:         ├── led-gray.gif
(2016-01-28 11:44:35) awehler:         ├── led-green.gif
(2016-01-28 11:44:35) awehler:         ├── led-orange.gif
(2016-01-28 11:44:35) awehler:         ├── led-purple.gif
(2016-01-28 11:44:41) awehler:         ├── led-red.gif
(2016-01-28 11:44:41) awehler:         ├── led-yellow.gif
(2016-01-28 11:44:41) awehler:         ├── tux.png
(2016-01-28 11:44:41) awehler:         └── Warning.svg
(2016-01-28 11:45:20) awehler: DOCROOT should be: ~/Noteboooks/docroot   # as suggested.
(2016-01-28 11:46:13) awehler: Address icons this way:     /icons/Warning.svg    # absolut addressed.
(2016-01-28 11:47:58) awehler: We have to keep in mind that "Notebooks/docroot" is reserved for stable shared contents in EBE zim context. No problem at all.
(2016-01-28 11:49:39) awehler: Git: still to be done.     # git init /tag/os/support/public/zim/docroot ; cd /tag/os/support/public/zim/docroot ; add . ; commit -m "Icons"
(2016-01-28 11:51:43) awehler: # git init /tag/os/support/public/zim/docroot ; cd /tag/os/support/public/zim/docroot ; git add . ; git commit -m "Icons"
'''









