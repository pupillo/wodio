Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2015-02-20T17:05:20+01:00

====== MovePage ======
Created Friday 20 February 2015

===== HowTo =====
	1. Goto destination page, on index panel
	2. Right-Click: **Copy** destination section link, on index panel
	3. Right-Click on source page on index panel: **Move Page**, insert section with Ctrl-V
