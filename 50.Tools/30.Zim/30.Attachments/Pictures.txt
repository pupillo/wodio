Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2015-05-15T06:46:00+02:00

====== Pictures ======
Created Friday 15 May 2015

===== Relative link for inserted image =====
	__From mailing list__
	
	On Thu, May 14, 2015 at 7:58 AM, Agustin Lobo <aloboaleu@gmail.com> wrote:
	> When I insert an image by dragging it to the page I get an absolute link.
	> Is there a way I could define the link to be relative by default in those
	> cases?
	> Thanks
	> Agus
	Problem solved by using Insert/Image and selecting "Attach Image First"
	Agus
	* **Alternative**: Attach file, {{./pasted_image.png?href=.%2Fpasted_image.png&width=100}}


===== Tip, link to image file =====
	* Put some image.png into attachment folder.
	* Open Attachments Folder
		* Select file
		* RightClick Properties
		* copy file name from dialog
	* Paste file path as text, like:
		# ./image.png
		# /dir1/dir.1.1/dir.1.1.1/image.png
	* Make path to link with
		# **Mark the path** and type **Ctrl-L**   # or:
		# **Mark the path** and select Menu entry:  **Insert - Link**













