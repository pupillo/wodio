# -*- coding: utf-8 -*-

# Copyright 2008 Jaap Karssenberg <jaap.karssenberg@gmail.com>

'''This plugin is to export the current page with one click'''

import gtk

from zim.fs import Dir, get_tmpdir
from zim.plugins import PluginClass
import zim.templates
from zim.exporter import Exporter

ui_xml = '''
<ui>
	<menubar name='menubar'>
		<menu action='file_menu'>
			<placeholder name='print_actions'>
				<menuitem action='quick_export'/>
			</placeholder>
		</menu>
	</menubar>
</ui>
'''

ui_actions = (
	# name, stock id, label, accelerator, tooltip, readonly
	('quick_export', 'gtk-network', _('_Quick Export'), '', 'Quickexport', True), # T: menu item

)

class QuickExportPlugin(PluginClass):

	plugin_info = {
		'name': _('Quick Export'), # T: plugin name
		'description': _('''\
This plugin provides a fast, single button method
to export the current page.  The export template 
and the locaton of the export are defined using the 
Configure dialog.  The export will use the full 
namespace.  Resources, like checkbox icons, will 
reference a _resources directory but the resource
itself will not be included in the export.  
'''), # T: plugin description
		'author': 'Nathan King',
		#'help': 'Plugins:Quick Export'
	}
	
	plugin_preferences = (
		('format', 'string', _('Format'), 'html'),
		('template', 'string', _('Template'), 'Print'),
		('dir', 'string', _('Export Directory'), '%s' % get_tmpdir()),
		('url', 'string', _('Resolved Prefix (optional)'), ''),
		('resolve_links', 'bool', _('Resolve resource links'), False),
		('namespace', 'bool', _('Use namespace directory structure'), False),
	)	

	def __init__(self, ui):
		PluginClass.__init__(self, ui)
		if self.ui.ui_type == 'gtk':
			self.ui.add_actions(ui_actions, self)
			self.ui.add_ui(ui_xml, self)

	def quick_export(self, page=None):
		self.ui.reload_page() # if page is not saved/reloaded, the exported html will be wrong
		file = self.export_page(page)
		self.ui.open_url(file)
			# Try to force web browser here - otherwise it goes to the
			# file browser which can have unexpected results
		
	def export_page(self, page=None):
		if not page:
			page = self.ui.page
		
		exformat = self.preferences['format'] or None
		extemplate = self.preferences['template'] or None
		exporter = Exporter(self.ui.notebook, exformat, extemplate)
		dir = Dir(self.preferences['dir']) 
		if self.preferences['resolve_links']:
			exporter.linker.target_dir = dir # resolves checkbox icons
			#exporter.linker.set_usebase(False) # doesn't work, attachments are always resolved
		exporter.export_page(dir, page, use_namespace=self.preferences['namespace'])
		
		path_url = self.preferences['url']
		if path_url == '':
			path_url = '%s' % dir
		
		path_page = '%s' % page.source
		if self.preferences['namespace']:
			path_notebook = '%s' % self.ui.notebook.dir
			html = path_page.replace(path_notebook, path_url)
		else:
			html = path_url + path_page[path_page.rfind('\\'):]
		html = html.replace('.txt', '.' + exporter.format.info['extension'])
		return html
		
		