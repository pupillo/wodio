Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2015-06-30T15:03:16+02:00

====== 40.Config ======
Created Tuesday 30 June 2015

===== Shared notebook: no =====
'''
In some zim config file
  notebook.zim

# you may want to adjust the flag
# This may be missing, then add it
  shared=False
#..
'''


===== Avoid: Shared notebook: yes =====
'''
notebook.zim
  shared=True     # if this is the case in any zim config file,
                  # check this path:
                  # ~/.cache/zim/notebook-*
#..
'''


===== Clear cache =====
'''
rm -fr ~/.cache/zim
#..
'''


===== Headings auto formatting =====
	{{./pasted_image.png?height=200}}
	# Edit / Preferences 
		# Automatically select the current word when you apply formatting
		# This will autoformat heading level 2 with Ctrl-2 only



