Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2017-03-08T08:21:18+01:00

====== 50.Editors ======
Created Wednesday 08 March 2017

===== About =====
	* https://www.toptal.com/it/programming-editors-a-never-ending-battle-with-no-clear-winner
		# __geany __preferred 


===== Wikis =====
	* http://www.wikimatrix.org		# overview
	* https://www.revolvy.com/main/index.php?s=Comparison of wiki software
	* https://foswiki.org/										# **home project** 
	* https://foswiki.org/System/UsersGuide
	* https://foswiki.org/System/FileAttachment
		{{./pasted_image.png?height=100&href=.%2Fpasted_image.png}}
	* https://foswiki.org/System/TipTopic019		# __hidden__ settings, with __html comment__ 


===== Markdown, standard =====
	* http://daringfireball.net/projects/markdown/		# Standard Markdown
	* http://daringfireball.net/projects/markdown/syntax	# Syntax

===== reStructuredText =====
	* https://en.wikipedia.org/wiki/ReStructuredText
	* RST
	* RsST
	* reST








