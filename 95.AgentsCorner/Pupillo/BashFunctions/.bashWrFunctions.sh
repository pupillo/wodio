# awehler: These are some of my favorite bash functions
# 2017-01-06

function l ()   { ls -al "$@" | cat ; }
function ll()   { ls -l "$@" | cat ; }
function Less() { less -NISR "$@" ; }
function Mess() { less -NISR +F --follow-name "$@" ; }
function Pgrep() { pgrep -fa "$@" ; }
function GetMyip () { lynx -dump http://checkip.dyndns.org/ \
  | head -n 1 | awk '{print $NF}'; }
function Pt()   { ps -L -A o \
user,tty,ppid,pid,tid,rtprio,nice,%cpu,%mem,stat,vsz:8,rss:8,stime,time,command ; }
# Ptn: sort by nice, highest prio last (negative, reverse order)
function Ptn()  { Pt | sort -n -k 7 -r ; }
# Ptr: sort by RT, highest prio last
function Ptr()  { Pt | sort -n -k 6 ; }
function Ps()   { ps f -A -o tty,ppid,pid,command ; }
function Pss()  { ps f -A -o \
  user,tty,ppid,pid,cpuid,%cpu,%mem,stat,vsz:8,rss:8,stime,time,command ; }

# ===== Timer function
# Timer now
# Timer "14:35" "Zeitschreibung machen"
# Timer "now + 1min"
# atq         # list jobs
# at -c 123   # cat job 123
function Timer() {
  xhost + 2>&1 > /dev/null
  local MSG=${2:-Timer}
  env -i bash --noprofile --norc -c " \
    echo \"/usr/bin/xclock -display :0.0 -title \\\"$MSG\\\" \
    -geometry 750x750 -bg red -fg white -hd white -hl white \
    -norender -update 1 \" | at $1 "
}

function Job_PurgeFiles() {
    find . \( -name "*~" -o -name "*.pyc" -o -regex ".*/core\.[0-9]*$" \) -print -exec rm {} \;
}


function AlertMe() {
  local title="$1 ($(date))"
  local rend="-norender"

  if [ x${OS} = xsolaris -o x${OS} = xSunOS ] ; then     # solaris is an exception
    rend=""
  fi

  xclock -title "$title" -geometry 750x750 \
         -bg red -fg white -hd white -hl white $rend -update 1 &
}
export -f AlertMe


function Date () {
    date "+%F %T"
}

# fill-region to fill column (defaul: 72)
function Par() {
  W=$1
  W=${W=72}
  cat | par -w $W ;
}
function YEd() {
  java -jar /usr/share/yed/yed.jar -open $1
}

function ZimFind() {
    local OPT="-Hn";
    local PAT;
    local OPTION;

    OPTIND=1;
    while getopts ":l" OPTION; do  # ": ..." keep quiet about errors
        case $OPTION in
            l)
                OPT="-sil"
            ;;
        esac;
    done;
    shift $((OPTIND-1));
    PAT=$1;
    if [ "x$PAT" = "x" ]; then
        echo "Usage:   ZimFind  <options> pattern";
        echo "Options:";
        echo "         -l       list files only, not matching lines";
        return;
    fi;
    {
        find ~/Notebooks/ \
          \( -name "*.txt" -o -name "*.svg" \) -exec grep -si ${OPT} "${PAT}" {} \;
    } 2> /dev/null
}
function P3() {
  python3 $@ 
}
function Pcd() {
  cd "$(python -c "import os.path as _, ${1}; \
    print _.dirname(_.realpath(${1}.__file__[:-1]))"
  )"
}

function PyFind() {
    DIR=$1
    shift
    find $DIR -name "*.py" -exec grep -sn $@ {} +
}
function ShutterClearHistory() {
  pkill -cf shutter
  sleep .5               # give shutter time to save session.xml et al

  rm -fr $HOME/.cache/shutter/unsaved
  rm -f $HOME/.shutter/session.xml
  rm -f $HOME/Snapshots/*
}

# TODO
# PyFind
# find $1 -name "*.py" -exec grep -sn $2 {} +
