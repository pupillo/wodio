Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2017-06-20T07:08:59+02:00

====== Tunnel ======
Created Tuesday 20 June 2017

===== About =====
	* https://docstore.mik.ua/orelly/networking_2ndEd/ssh/ch09_02.htm
		# O'Reilly book, Port Forwarding
	* https://www.openssh.com/manual.html		# OpenSSH man pages
	* http://man.openbsd.org/ssh
	* [[./tunneling.svg]] 		# example
		{{./pasted_image005.png?height=100&href=.%2Fpasted_image005.png}}


===== Examples =====
	* http://www.kerrybuckley.org/2008/12/15/chaining-ssh-tunnels/
	* http://www.jedi.be/blog/2008/11/07/chaining-ssh-tunnels-easy-ssh-hopping/
	* http://www.revsys.com/writings/quicktips/ssh-tunnel.html	# 2 examples

===== Forums =====
	* https://superuser.com/questions/96489/an-ssh-tunnel-via-multiple-hops
		{{./pasted_image.png?height=80&href=.%2Fpasted_image.png}}, {{./pasted_image001.png?height=80&href=.%2Fpasted_image001.png}}


===== Tutorials =====
	* https://chamibuddhika.wordpress.com/2012/03/21/ssh-tunnelling-explained/		# __goood__
	* https://bioteam.net/2009/10/ssh-tunnels-for-beginners/
	* https://bioteam.net/2009/11/ssh-tunnels-part-2/
	* https://bioteam.net/2009/11/ssh-tunnels-part-3-reverse-tunnels/
	* https://www.bitvise.com/port-forwarding		# **diagrams (!)** 
		{{./pasted_image002.png?height=80&href=.%2Fpasted_image002.png}}, {{./pasted_image003.png?height=80&href=.%2Fpasted_image003.png}}, {{./pasted_image004.png?height=80&href=.%2Fpasted_image004.png}}
		''# ssh -L999:127.0.0.1:123 user1@10.2.2.3 -N 127.0.0.1'' 


===== Auto closing =====
	* http://www.g-loaded.eu/2006/11/24/auto-closing-ssh-tunnels/		
# __goood__
	* https://gist.github.com/scy/6781836	# __goood__

